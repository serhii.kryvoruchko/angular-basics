import { Component } from "@angular/core";
import { bindCallback } from "rxjs";


@Component({
   selector: 'app-post4',
   template: `
   <div class ="post4">
      <h2>post4</h2>
      <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum cupiditate dolorum atque dolorem veritatis sed aperiam, earum quidem. Adipisci est soluta possimus! Dolores quaerat, 
            consequatur officia eligendi illum pariatur? Vero!
      </p>
   </div>
   `,
   styles: [`
      .post4{
         padding: .5rem;
         border:2px solid #aaa;
         border-radius:10px;
      }
   `]

})
export class Post4Component {

}